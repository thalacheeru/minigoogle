/**  
 *  
 * This program is part of an implementation for the Mini-Google project which is 
 * about developing a search engine using efficient Datastructures.
 * 
 * Created by Mahender K on 04-10-2009
 */

package searchengine.dictionary;
import java.util.*;


public class HashDictionary <K extends Comparable<K>, V> implements DictionaryInterface <K,V>
{

	//The ht as the Hashtable for this HashDictionary
	private Hashtable<K, V> ht;
	//Hashtable<K,V> ht=new Hashtable<K,V>();
	int size=0,i=0;
	//K[] Keys;
	/**
	 * Constructs a new, empty hashtable with a default initial capacity (11)
	 * and load factor, which is <tt>0.75</tt>.
	 */
	public HashDictionary(){
		ht = new Hashtable<K, V>();
	}
	/**
	 * getKeys functions returns all the Keys present in the 
	 * HashDictionary as a String Array.
	 * @return all the Keys in a String Array
	 */
	public K[] getKeys() {
		K[] keys = (K[])new String[ht.size()];
		Enumeration<K> enu = ht.keys();
		for(int i=0;enu.hasMoreElements();i++){
			keys[i]=enu.nextElement();
		}
		return keys;
	}


	/**
	 * getValue returns the value for the key(str) if found in the hashtable
	 * else returns null
	 * @param key is the key for which value is to be returned 
	 * @return the value of the key.
	 */
	public V getValue(K key) {
		return ht.get(key);
	}


	/**
	 * Maps the specified <code>key</code> to the specified 
	 * <code>value</code> in this HashDictionary. Neither the key nor the
	 * value can be <code>null</code>. <p>
	 *
	 * The value can be retrieved by calling the <code>getValue</code> method
	 * with a key that is equal to the original key. 
	 *
	 * @param      key     the HashDictionary key.
	 * @param      value   the value.
	 *
	 * @see     #getValue(String)
	 */
	public void insert(K key, V value) {
		ht.put(key, value);
	}


	/**
	 * remove function removes the key and value from the HashTable
	 * if found in the hashtable
	 * @param key
	 */
	public void remove(K key) {
		ht.remove(key);
	}

	
}
/*import java.util.Enumeration;
import java.util.*;

public class HashDictionary <K extends Comparable<K>, V> implements DictionaryInterface <K,V>
{
	Hashtable<K,V> ht=new Hashtable<K,V>();
	int size=0,i=0;
	K[] allKeys;
	public void insert(K key, V value) 
	{
		ht.put(key, value);
		size++;
	}
	public K[] getKeys() 
	{
		allKeys = (K[]) new String[ht.size()];
		Enumeration<K> keys;

		keys = ht.keys();
		
		while (keys.hasMoreElements()) 
		{
			allKeys[i]= keys.nextElement();
			i++;
		}
		return  allKeys;
	}
	public void remove(K key) 
	{
		ht.remove(key);
		size--;
	}
	public V getValue(K str) 
	{
		return ht.get(str);
	}

}
*/