package searchengine.dictionary;

public class ParenthesisMatching 
{
	public static void main(String args[])
	{
		int x=0;
		SLStack<String> ss = new SLStack<String>();

		ss.push("(");
		ss.push("a");
		ss.push("+");
		ss.push("(");
		ss.push("b");
		ss.push("*");
		ss.push("+");
		ss.push("(");
		ss.push("c");
		ss.push("+");
		ss.push("d");
		ss.push(")");
		ss.push(")");
		ss.push(")");
		ss.show();
		x=ss.check();
		if(x==1)
			System.out.println("\nIt Is A Balanced Equation");
		else
			System.out.println("\nIt Is Not A Balanced Equation");
	}
}
class SLStack<E> 
{
	String s;
	SLLNode<E> head;
	public SLStack()
	{
		head = new SLLNode<E>();
	}
	public void push(E element)
	{
		SLLNode<E> newnode = new SLLNode<E>(element);
		newnode.setNext(head);
		head = newnode;
	}
	public void show()
	{
		SLLNode<E> temp = head;
		while(temp.getNext() != null)
		{
			s=s+temp.getData();
			temp=temp.getNext();
		}
		int a=s.length();
		System.out.println("Entered String is\n");
		for(int i=a-1;i>3;i--)
			System.out.print(s.charAt(i));
	}
	public int check()
	{
		SLLNode<E> temp=head;
		int rp=0,lp=0;
		while(temp.getNext() != null)
		{
			if(temp.getData().equals("("))
				rp++;
			else 
				if(temp.getData().equals(")"))
					lp++;
			temp=temp.getNext();
		}
		if(rp==lp)
			return 1;

		else 
			return 0;

	}
}
class SLLNode<E>
{
	private E Data;
	private SLLNode<E> next;
	SLLNode() 
	{
		Data = null;
		next = null;
	}
	SLLNode(E data)
	{
		Data = data;
		next = null;
	}
	public E getData()
	{
		return Data;
	}
	public void setData(E data)
	{
		Data = data;
	}
	public SLLNode<E> getNext()
	{
		return next;
	}
	public void setNext(SLLNode<E> next)
	{
		this.next = next;
	}
}

