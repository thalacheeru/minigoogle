
/**  
 * 
 * Copyright: Copyright (c) 2004 Carnegie Mellon University
 * 
 * This program is part of an implementation for the PARKR project which is 
 * about developing a search engine using efficient Datastructures.
 * 
 * Modified by Mahender on 12-10-2009
 */
package searchengine.spider;

import java.io.*;
import java.net.*;
import java.util.*;

import queue.CAQueue;
import queue.LLQueue;
import searchengine.dictionary.ObjectIterator;
import searchengine.element.*;
import searchengine.indexer.Indexer;
import searchengine.parser.PageLexer;
import searchengine.url.URLTextReader;
//import DS.Queue.CAQueue;
/** Web-crawling objects.  Instances of this class will crawl a given
 *  web site in breadth-first order.
 */
public class BreadthFirstSpider implements SpiderInterface
{
	/** Create a new web spider.
	@param u The URL of the web site to crawl.
	@param i The initial web index object to extend.
	 */
	URL u;
	public BreadthFirstSpider (URL u, Indexer i)
	{
		this.u = u;
		this.i = i;
	}

	private Indexer i = null;


	LLQueue q=new LLQueue();


	/** Crawl the web, up to a certain number of web pages.
	@param limit The maximum number of pages to crawl.
	 */
	public Indexer crawl (int limit)
	{
		int temp1=1;
		int cnt=0,cnt1=1;
		try
		{	
			URLTextReader in;
			//System.out.println("Hai4");
			while(u != null && cnt1<=10)
			{
				System.out.println(temp1+"  URL : "+u);
				temp1++;
				//System.out.println("New URL");
				Vector vec = new Vector();
				in = new URLTextReader(u);
				//System.out.println("Hai5");
				//System.out.println("While :"+u);
				try
				{
					PageLexer<PageElementInterface> elt = new PageLexer<PageElementInterface>(in, u);
					//System.out.println("Hai6");
					while(elt.hasNext())
					{
						//System.out.println("Hai5");
						PageElementInterface p=elt.next();

						if(p instanceof PageHref){
							//ht.put(key, value);
							q.enqueue(p);
							cnt++;
							System.out.println("Sub : "+p);
						}
						else if(p instanceof PageWord)
						{
							//System.out.println("Word : "+p);
							vec.add(p);
						}
					}
					//System.out.println("\n\n\n");
				}catch(Exception e){}
				//q.Display();
				/*System.out.println("123456789");
				for(int kk=0;kk<vec.size();kk++){
					//
					System.out.println("       "+vec.get(kk));}*/
				//System.out.println("  New : ");
				ObjectIterator oi = new ObjectIterator(vec);
				i.addPage(u,oi);

				PageElementInterface temp=(PageElementInterface) q.dequeue();
				u=new URL(temp.toString());
				
				cnt1++;
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		return i;
	}
	/** Crawl the web, up to the default number of web pages.
	 */
	public Indexer  crawl()
	{
		// This redirection may effect performance, but its OK !!
		System.out.println("Crawling: "+u.toString());
		return  crawl(crawlLimitDefault);
	}
	/** The maximum number of pages to crawl. */
	public int crawlLimitDefault = 10;
}


