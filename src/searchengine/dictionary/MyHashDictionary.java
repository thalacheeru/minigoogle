
package searchengine.dictionary;

public class MyHashDictionary <K extends Comparable<K>, V> implements DictionaryInterface <K,V>{

    private ListNode[] index;
    int size;
    
    public MyHashDictionary()
    {
    	index = new ListNode[5]; 
    }
    
    private void resizetable() 
    {
        ListNode[] newhashtable = new ListNode[index.length*2];
        for (int i = 0; i < index.length; i++)
        {
              
           ListNode list = index[i];
           while (list != null) 
           {
              ListNode next = list.next;  
              int hash = (Math.abs(hashcode((K)list.key))) % newhashtable.length;
                
              list.next = newhashtable[hash];
              newhashtable[hash] = list;
              list = next; 
           }
        }
        index = newhashtable; 
     } 

    private int hash(K key)
    {
    	 return (Math.abs(hashcode(key))) %index.length;
    }
    
	public K[] getKeys() {
		 
		 String[] s=new String[size];
		 int j=0;
		 for (int i = 0; i < index.length; i++) 
		 {
              ListNode list = index[i]; 
              while (list != null) 
              { 
                s[j]=(String) list.key;
                list = list.next;
                j++;
              }
         }
		return (K[])s;
	}


	public V getValue(K key) 
	{
		
		 int size = hash(key);  
	      
	      ListNode list = index[size];  
	      while (list != null) 
	      {
	         if (list.key.equals(key))
	            return (V)list.value;
	         list = list.next; 
	      }
	    
	      return null;  
	}

	public int hashcode(K k)
	{
		String str=(String)k;
		int res=0;
		for(int i=0;i<str.length();i++)
		{
			res=(int)(res+str.charAt(i)*Math.pow(37,str.length()-(i+1)));
		}
		return res;
	}

	public void insert(K key, V value) 
	{
	     int size = hash(key); 
	      
	     ListNode list = index[size]; 
	     while (list != null) 
	     {
	         if (list.key.equals(key))
	            break;
	         list = list.next;
	     }
	      
	     if (list != null) 
	     {
	         list.value = value;
	     }
	     
	     else 
	     {
	         if (size >= 0.75*index.length) 
	         {
	            
	         }
	         ListNode newone = new ListNode();
	         newone.key = key;
	         newone.value = value;
	         newone.next = index[size];
	         index[size] = newone;
	         size++;  
	    }
	}


	public void remove(K key) {
		
		   int size = hash(key);  
		      
		      if (index[size] == null) {
		         return; 
		      }
		      
		      if (index[size].key.equals(key)) {
		    	  index[size] = index[size].next;
		        size--; 
		         return;
		      }
		      
		      
		      ListNode prev = index[size];  
		      ListNode curr = prev.next; 
		      while (curr != null && ! curr.key.equals(key)) {
		         curr = curr.next;
		         prev = curr;
		      }		      
		    
		      if (curr != null) {
		         prev.next = curr.next;
		         size--;  
		      }	
	}
}
