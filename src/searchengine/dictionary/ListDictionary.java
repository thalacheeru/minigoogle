package searchengine.dictionary;


class ListNode<K extends Comparable<K>, V>
{
	K key;
	V value;
	ListNode<K, V> next;
}
public class ListDictionary <K extends Comparable<K>, V> implements DictionaryInterface <K,V>
{
	ListNode<K,V> temp, head = new ListNode<K,V>(); 
	int size = 0;
	K[] keys;
	@SuppressWarnings("unchecked")

	public K[] getKeys()
	{
		keys=  (K[]) new String[size];
		temp = head.next;
		int i = 0;
		if(head.next==null)
			//System.out.println("List Is Empty ");
			try 
		{
				throw new Exception("List empty");
		}
		catch (Exception e) 
		{

			e.printStackTrace();
		}

		while(temp.next!=null)
		{
			keys[i] = temp.key;
			i++;
			temp = temp.next;
		}
		keys[i]=temp.key;
		return keys;
	}
	public V getValue(K str) 
	{
		temp = head.next;
		String a=null;
		while(temp.next!=null)
		{
			if(temp.key.equals(str))
			{
				a=temp.value.toString();
				break;
			}
			else
				temp = temp.next;
		}
		return (V) a;
	}
	public void insert(K key, V value)
	{
		ListNode<K,V> main = new ListNode<K,V>();
		main.key = key;
		main.value = value;
		int flag = 0;
		if(head==null)
		{
			head = main;
			size++;
		}
		else
		{
			temp = head;
			while(temp.next!=null)
			{
				temp = temp.next;
				if(temp.key.equals(main.key))
				{
					temp.value = main.value;
					flag = 1;
					break;
				}
			}
			if(flag==0)
			{
				temp.next = main;
				size++;
			}
		}
	}
	public void remove(K key) {
		temp = head; 
		ListNode<K,V> t = null;
		while(temp.next!=null)
		{
			t = temp;
			temp = temp.next;
			if(temp.key.equals(key))
				temp.next=null;
			break;
		}
		t.next = temp.next;
		size--;
	}
}
