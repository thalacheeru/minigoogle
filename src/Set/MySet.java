package Set;

import java.util.Iterator;
import java.util.Vector;

public class MySet<T> implements SetADT<T> {
	Vector<T> collection;
	
	public MySet(){
		collection = new Vector<T>();
	}
	public void add(T element) {
		 
		collection.add(element);
	}

	public boolean contains(T target) {
		 
		return collection.contains(target);
	}

	public SetADT<T> difference(SetADT<T> set) {
		 
		Iterator<T> itr1 = collection.iterator();
		MySet<T> temp = new MySet<T>();
		while(itr1.hasNext()){
			T ele = itr1.next();
			boolean isexist = set.contains(ele);
			if(!isexist){
				temp.add(ele);
			}
		}
		return temp;
	}

	public boolean equals(SetADT<T> set) {
		 
		Iterator<T> itr1 = collection.iterator();
		while(itr1.hasNext()){
			T ele = itr1.next();
			boolean isexist = set.contains(ele);
			if(!isexist){
				return false;
			}
		}
		return true;
	}

	public SetADT<T> intersection(SetADT<T> set) {
		 
		Iterator<T> itr1 = collection.iterator();
		MySet<T> temp = new MySet<T>();
		while(itr1.hasNext()){
			T ele = itr1.next();
			boolean isexist = set.contains(ele);
			if(isexist){
				temp.add(ele);
			}
		}
		return temp;
	}

	public boolean isEmpty() {
		
		return collection.isEmpty();
	}

	public Iterator<T> iterator() {
		
		return collection.iterator();
	}

	public T remove(T element) {
		
		int index = collection.indexOf(element);
		return collection.remove(index);
	}

	public int size() {
		
		return collection.size();
	}

	public SetADT<T> union(SetADT<T> set) {
		// TODO Auto-generated method stub
		Iterator<T> itr1 = collection.iterator();
		MySet<T> temp = new MySet<T>();
		while(itr1.hasNext()){
			T ele = itr1.next();
			temp.add(ele);			
		}
		itr1 = set.iterator();
		while(itr1.hasNext()){
			T ele = itr1.next();
			boolean isexist = temp.contains(ele);
			if(!isexist){
				temp.add(ele);
			}	
		}
		return temp;
	}

}

