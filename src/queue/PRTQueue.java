package queue;

public class PRTQueue<E extends Comparable<E>> implements PQueueADT<E> {

	@SuppressWarnings("unchecked")
	E[] myArr=(E[])new String[20]; int s=1;int temp1,temp2,lastTemp,iFlag,index=0;
	@Override
	public void enqueue(E value) {

		temp1=2*s;temp2=(2*s)+1;
		if((2*s)+1<21)
		{
			if(myArr[1]==null) 
			{
				myArr[s]=value;
			}
			else
			{

				//System.out.println("----"+value);
				if(myArr[temp1]==null)
				{
					myArr[temp1]=value;
					lastTemp=temp1;
					//System.out.println("Parent: "+myArr[temp1/2]+" value: "+value);
				}
				else if(myArr[temp2]==null)
				{
					myArr[temp2]=value;
					lastTemp=temp2;
					//System.out.println("Parent: "+myArr[temp2/2]+" value: "+value);
				}
				if(myArr[temp1]!=null && myArr[temp2]!=null)
					s++; 


				myArr=swap(lastTemp);

			}
		}
		//System.out.println(myArr[s]);

	}

	public int depth(String s)
	{
		int j=0,count=-2;
		//System.out.println(links[m]);
		while(j<s.length()){
			if(s.charAt(j)=='/'){
				count++;
			}
			j++;
		}
		return count;

	}


	public E[] swap(int num)
	{


		if(depth(myArr[num].toString())>(depth(myArr[num/2].toString())))
		{
			//System.out.println("@@@@"+myArr[num]+"--"+myArr[num/2]);
			E val=myArr[num];
			myArr[num]=myArr[num/2];
			myArr[num/2]=val;

			if(num/2>=1)
				swap(num/2);
		}

		return myArr;

		/*if(myArr[temp2].compareTo(myArr[s])<0)
		{
			E val=myArr[temp2];
			myArr[temp2]=myArr[temp2/2];
			myArr[temp2/2]=val;

		}*/

	}
	@Override
	public E dequeue() {
		E dVal=myArr[1];
		myArr[1]=myArr[lastTemp];
		myArr[lastTemp]=null;
		iFlag=0;
		myArr=dSwap();
		lastTemp--;

		return dVal;
	}
	//int index=1;
	private E[] dSwap()
	{
		//System.out.println("===="+myArr[1]);
		if(iFlag!=1){
			index=1;
			iFlag=1;}
		//		System.out.println(index);
		//		System.out.println("@@"+myArr[index]);
		//		System.out.println("child "+myArr[2*index]+"----"+myArr[(2*index)+1]);
		if(myArr[(2*index)]!=null&&myArr[(2*index)+1]!=null)
		{
			if(myArr[index].compareTo(myArr[2*index])>0)
			{
				E sVar=myArr[index];
				myArr[index]=myArr[index*2];
				myArr[index*2]=sVar;
				index=2*index;
			}
			else if(myArr[index].compareTo(myArr[(2*index)+1])>0)
			{
				E sVar=myArr[index];
				myArr[index]=myArr[(2*index)+1];
				myArr[(2*index)+1]=sVar;
				index=(2*index)+1;
			}
			if(myArr[index]!=null)
			{
				dSwap();
			}
		}
		return myArr;
	}

	@Override
	public int size() {
		
		int count=0;
		for(int i=1;i<myArr.length;i++)
		{
			if(myArr[i]!=null)
				count++;
		}
		return count;
	}

	@Override
	public boolean is_empty() {
		
		if(myArr[1]==null)
			return true;

		return false;
	}

	@Override
	public E front() {
		
		return myArr[1];
	}

	public void display()
	{
		for(int k=1;k<myArr.length;k++)
			if(myArr[k]!=null)
				System.out.println(myArr[k]);
	}

	/*public static void main(String args[])
	{
		PQueueImp<String> ob=new PQueueImp<String>();
		ob.enqueue("abc");
		ob.enqueue("5");
		ob.enqueue("8");
		ob.enqueue("3");
		ob.enqueue("9");
		ob.enqueue("6");
		System.out.println("Initial state: ");
		System.out.println("Front: "+ob.front());
		System.out.println("Is empty: "+ob.is_empty());
		System.out.println("Size: "+ob.size());
		ob.display();

		System.out.println("");
		ob.dequeue();
		System.out.println("AFter Dequeue: ");
		System.out.println("Front: "+ob.front());
		System.out.println("Is empty: "+ob.is_empty());
		System.out.println("Size: "+ob.size());
		ob.display();

		System.out.println("");
		ob.dequeue();
		System.out.println("Once again AFter Dequeue: ");
		System.out.println("Front: "+ob.front());
		System.out.println("Is empty: "+ob.is_empty());
		System.out.println("Size: "+ob.size());
		ob.display();
	}*/

}
