package searchengine.search;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;

import java.awt.Color;

import javax.swing.JTextPane;

import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JScrollBar;
import javax.swing.border.EmptyBorder;

public class Clientsocket extends JFrame 
{
	private JPanel contentPane;
	private JTextField textField;
	private JTextArea fid;
	private JTextArea textArea;
	private JTextField wid;
	private JComboBox comboBox;
	private JTextField filename;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clientsocket frame = new Clientsocket();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Clientsocket() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 610, 479);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Clear");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				filename.setText(" ");
				wid.setText(" ");
				//comboBox.setToolTipText("Select");
			}
		});
		btnNewButton.setBounds(441, 227, 89, 23);
		contentPane.add(btnNewButton);

		JButton sid = new JButton("Search");
		sid.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				//				try
				//				{
				//					//Socket smtpSocket = null;  
				//					DataOutputStream os = null;
				//					DataInputStream is = null;
				//					String file,word,dict="null";
				//					file=fid.getText();
				//					System.out.println(file);
				//					word=wid.getText();
				//					System.out.println(word);
				//					//System.out.println(comboBox.getSelectedItem().toString());
				//
				//					String arg=file+" "+word;
				//					System.out.println(arg);
				//					Socket s=new Socket("localhost",1111);
				//					server a=new server();
				//					PrintStream ps=new PrintStream(s.getOutputStream());
				//					ps.print(arg);;
				//					//a.retrieve(arg);
				//
				//
				//
				//				}
				try 
				{   
					String file,word,dict="null";
					file=filename.getText();
					System.out.println("File : "+file);
					word=wid.getText();
					System.out.println("word : "+word);
					//System.out.println(comboBox.getSelectedItem().toString());

					String arg=file+" "+"list"+" "+word;
					System.out.println("Arg "+arg);
					Socket client = new Socket("localhost",2007);
					System.out.println("Port Success");
					PrintStream sendRequest = new PrintStream(client.getOutputStream());
					System.out.println("Print Stream");
					sendRequest.println(arg);
					BufferedReader b = new BufferedReader(new InputStreamReader(client.getInputStream()));
					String s = b.readLine();
					if(s!=null)
					{
						System.out.println("displaying output");
						System.out.println("s"+s);
						String temp=s;
						StringTokenizer st = new StringTokenizer(s,"~~");

						//for (int i = 0 ; i < args.length ; i++)
						int i = 0;

						while(st.hasMoreTokens())
						{
							String s2 = st.nextToken();
							i++;
							//uid.setText(s2);

						}
						String [] str=new String[i];
						i=0;
						StringTokenizer stt = new StringTokenizer(temp,"~~");
						String s2="";
						while(stt.hasMoreTokens())
						{
							s2 = s2+stt.nextToken()+"\n";

							//str[i]=stt.nextToken();;
							//i++;

						}
						//System.out.println("Hai "+s2);
						//s2=s2;
						//s2=s2;
						System.out.println(s2);
						fid.setText("  ");  
						fid.append(s2);



						// uid.setText(new String(" "));
						//  uid.append(s2);
						sendRequest.close();}else
							System.out.println("Empty");
					b.close();
					client.close();
				} catch (IOException ex) 
				{
					//Logger.getLogger(ClientInterface.class.getName()).log(Level.SEVERE, null, ex);
					ex.printStackTrace();
				}
			}
		});
		sid.setBounds(111, 227, 89, 23);
		contentPane.add(sid);

		fid = new JTextArea();
		fid.setBounds(123, 281, 380, 123);
		contentPane.add(fid);


		JLabel lblNewLabel = new JLabel("Filename");
		lblNewLabel.setBounds(176, 128, 77, 30);
		contentPane.add(lblNewLabel);

		JLabel lblDictionary = new JLabel("Dictionary");
		lblDictionary.setBounds(176, 166, 77, 19);
		contentPane.add(lblDictionary);

		wid = new JTextField();
		wid.setBounds(310, 196, 86, 20);
		contentPane.add(wid);
		wid.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Word");
		lblNewLabel_1.setBounds(174, 202, 60, 14);
		contentPane.add(lblNewLabel_1);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select", "bst", "hash", "myhash"}));
		comboBox.setBounds(310, 165, 86, 20);
		contentPane.add(comboBox);

		filename = new JTextField();
		filename.setBounds(309, 133, 86, 20);
		contentPane.add(filename);
		filename.setColumns(10);

		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(513, 281, 17, 123);
		contentPane.add(scrollBar);

		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(265, 39, 46, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("D:\\Workspace\\Minigoogle\\src\\searchengine\\search\\sp1.JPG"));
		lblNewLabel_2.setBounds(183, 11, 268, 117);
		contentPane.add(lblNewLabel_2);


	}
}