/**  
 * 
 * Copyright: Copyright (c) 2004 Carnegie Mellon University
 * 
 * This program is part of an implementation for the PARKR project which is 
 * about developing a search engine using efficient Datastructures.
 * 
 * Created by Mahender on 12-10-2009
 */
package searchengine.spider;

import java.net.*;
import java.util.ArrayList;
import java.util.Vector;

import queue.PRTQueue;
import searchengine.dictionary.ObjectIterator;
//import searchengine.dictionary.PRTqueue;
import searchengine.element.PageElementInterface;
import searchengine.element.PageHref;
import searchengine.element.PageWord;
import searchengine.indexer.Indexer;
import searchengine.parser.PageLexer;
import searchengine.url.URLTextReader;

/**
 * Web-crawling objects. Instances of this class will crawl a given web site in
 * Priority-first order.
 */
public class PriorityBasedSpider implements SpiderInterface 
{
	/**
	 * Create a new web spider.
	 * 
	 * @param u
	 *            The URL of the web site to crawl.
	 * @param i
	 *            The initial web index object to extend.
	 */

	private Indexer i = null;
	private URL u;

	public PriorityBasedSpider(URL u, Indexer i) {
		this.u = u;
		this.i = i;

	}
	/*System.out.println("===" + temp_url);
	try {
		if (temp_url != null) // && limit>0
		{
			u = new URL(temp_url.toString());
		} else {
			temp_url = ql.dequeue();
			u = new URL(temp_url.toString());
		}
	} catch (MalformedURLException e) {
		// System.out.println();
	}*/
	/**
	 * Crawl the web, up to a certain number of web pages.
	 * 
	 * @param limit
	 *            The maximum number of pages to crawl.
	 *            
	 */
	PRTQueue<String> ql = new PRTQueue<String>();
	public Indexer crawl(int limit) {

		int count = limit;
		URLTextReader in;
		
		Vector<String> vec = new Vector<String>();
		ArrayList<PageElementInterface> arr = new ArrayList<PageElementInterface>();

		try {
			while (u != null) {
				System.out.println("Main : " + u);
				in = new URLTextReader(u);

				try
				{
					PageLexer<PageElementInterface> pl = new PageLexer<PageElementInterface>(in, u);
					while (pl.hasNext())
					{

						PageElementInterface p = pl.next();

						if (p instanceof PageHref)
						{

							if (!arr.contains(p.toString())) 
							{

								System.out.println("Sub : "+p);
								arr.add(p);
								if (count > 0)
								{
									//System.out.println("Hai");
									ql.enqueue(p.toString());
								}

								count--;

							}
							 
						} else if (p instanceof PageWord) 
						{
							//System.out.println("Else :"+g);
							vec.add(p.toString());
						}

					}
				}
				catch (Exception e)
				{
				}
				ObjectIterator<String> obt = new ObjectIterator<String>(vec);
				System.out.println("URL: " + u);
				i.addPage(u, obt);
				System.out.println("hello");
				PRTQueue<String> q1;
				String temp_url = ql.dequeue();
				//PageElementInterface temp_url=(PageElementInterface) q1.dequeue();
				u=new URL(temp_url.toString());

				System.out.println("New :"+temp_url); 			} 
			System.out.println("hello");
			 
		} catch (Exception e) {

		}

		return i;
	}

	/**
	 * Crawl the web, up to the default number of web pages.
	 */
	public Indexer crawl() {
		// This redirection may effect performance, but its OK !!
		System.out.println("Crawling: " + u.toString());
		return crawl(crawlLimitDefault);
	}

	/** The maximum number of pages to crawl. */
	public int crawlLimitDefault = 10;

}

