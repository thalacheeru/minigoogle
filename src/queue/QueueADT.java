package queue;

public interface QueueADT<T>
{
	public T  front();
	public T enqueue(T element);
	public T dequeue();
	public boolean isempty();	
	public int size();
	public void Display();

}

