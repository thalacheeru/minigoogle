
package searchengine.dictionary;

class Node<K extends Comparable<K>, V>
{
	Node<K, V> left,right;
    K key;
	V value;
	
	public Node(){}
	public Node(K k,V v)
	{
		key=k;
		value=v;
	}
  }

public class BSTDictionary <K extends Comparable<K>, V> implements DictionaryInterface <K,V>{
    
	Node root;
	int size;
	String[] s;
	int i;
	V get;
	
	public BSTDictionary()
	{
	  size=0;	
	}
	
	public K[] getKeys() 
	{
		s=new String[size];
		i=0;
        if(size==0)
        	return (K[])s;
		s=print(root);
        return (K[])s;
        
	}
	
	public String[] print(Node temp)
	{    
		 
		 if (temp != null) 
		 {      
                print(temp.left);
	            s[i++]=(String)temp.key;
	            print(temp.right);
	     }
		 
		 return s;
	}

	
	public V getValue(K str) {
		
		Node<K,V> temp=root;
		while(temp!=null)
		{
			if(str.compareTo(temp.key)<0)
			{
				temp=temp.left;
			}
			else if(str.compareTo(temp.key)>0)
			{
				temp=temp.right;
			}
			else
			{
				return temp.value;
			}
		}
		return null;
	}
	
	public void insert(K key, V value)
	{
		if(size==0)
		{
			root=new Node(key,value);
		    size++;
		}
		else
		{
			inserts(root,key,value);
		}
		   
	}
    
	public void inserts(Node temp,K key,V value)
	{
		int cmp=key.compareTo((K) temp.key);
		if(cmp<0)
		 {
			if (temp.left != null) 
			{  
		        inserts(temp.left,key,value);
		    }
			else 
			{   
		        
		        temp.left = new Node(key,value);
		        size++;
		        return;
		    }
		 }	
		 else if (cmp>0) 
		 {
		      if (temp.right != null) 
		      { 
		    	  inserts(temp.right,key,value);
		      } 
		      else
		      {
		         
		        temp.right = new Node(key,value);
		        size++;
		        return;
		      }
		 }
		 else if(cmp==0)
		 {
			 temp.value=value;
		 }
	}

	public void remove(K key) 
	{
        Node current = root;
        Node parent = root;
        boolean isleft = true;
        while(!current.key.equals(key))
        {
        	parent=current;
        	if(key.compareTo((K) current.key)<0)
        	{
        		isleft=true;
        		current=current.left;
        	}
        	else
        	{
        		isleft=false;
        		current=current.right;
        	}
        	if(current==null)
        	return;
        }
        if(current.left==null && current.right==null)
        {
            if(current == root)
                root = null;
            else if(isleft)
                parent.left = null;
            else
                parent.right = null;
           
        }
        else if(current.right==null)
        {
            if(current == root)
                root = current.left;
            else if(isleft)
                parent.left = current.left;
            else
                parent.right = current.left;
           
        }
        else if(current.left==null)
        {
            if(current == root)
                root = current.right;
            else if(isleft)
                parent.left= current.right;
            else
                parent.right = current.right;
           
        }
       
        else
        {
            Node child = findchildren(current);
            if(current == root)
                root =child ;
            else if(isleft)
                parent.left =child ;
            else
                parent.right =child ;
            child .left = current.left;
            
        } 
        
        size--;
    }
  
	private Node findchildren(Node temp)
     {
        Node childParent = temp; 
        Node child = temp;
        Node current = temp.right;
        while(current != null)
        {
           childParent = child;    
           child = current;
           current = current.left;
        }
       
        if(child != temp.right)
         {
            childParent.left= child.right;
            child.right = temp.right;
         }
       return child;
    }
}	