/**  
 * 
 * Copyright: Copyright (c) 2004 Carnegie Mellon University
 * 
 * This program is part of an implementation for the PARKR project which is 
 * about developing a search engine using efficient Datastructures.
 * 
 * 
 */ 

package searchengine.element;

import java.net.MalformedURLException;
import java.net.URL;

import searchengine.url.URLFixer;

/** A img src in a web page.
 *
 */
public class PageImg implements PageElementInterface {

	public PageImg (String h) throws MalformedURLException /* perhaps throw an invalid filename error? */{
		img = new URL(h);
	}
	public PageImg (URL context, String h) throws MalformedURLException {
		img = URLFixer.fix(context, h);
	    }
	public  String toString () {
		return img.toString();
	}

	private URL img;
}
