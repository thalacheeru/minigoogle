/*package searchengine.dictionary;

import java.util.*;
import java.io.*;
class PriorityQueue
{
	node front,temp, current,previous;
	boolean insert(String a, int b){
		node nNode = new node(a,b);
		if(front == null){
			front = nNode;
			return true;
		}else
		{
			current=front;
			previous = front;
			if(front.priority>b){
				nNode.next=front;
				front = nNode;
				return true;
			}
			while (current.next!=null && current.priority<=b)
			{
				previous = current;
				current = current.next;
			}
			if(current.priority>b){
				nNode.next=current;
				previous.next=nNode;
				return true;
			}else{
				current.next = nNode;
				return true;
			}


		}
		//return true;
	}
	void display(){
		current = front;
		while (current!=null)
		{
			System.out.println(current.ID+"\t\t\t"+current.priority);
			current=current.next;
		}
	}
}
class node
{
	String ID;
	int priority;
	node next;
	node(String a, int b){
		ID = a;
		priority = b;
	}
}*/
package searchengine.dictionary;

public class PRTQueue<E extends Comparable<E>> implements PQueueADT<E> {

	@SuppressWarnings("unchecked")
	E[] arr = (E[]) new String[20];
	int s = 1;
	int temp1, temp2, lastTemp, iFlag, index = 0;

	@Override
	public void enqueue(E value) {

		temp1 = 2 * s;
		temp2 = (2 * s) + 1;
		if ((2 * s) + 1 < 21) {
			if (arr[1] == null) {
				arr[s] = value;
			} else {

				// System.out.println("----"+value);
				if (arr[temp1] == null) {
					arr[temp1] = value;
					lastTemp = temp1;
					// System.out.println("Parent: "+myArr[temp1/2]+" value: "+value);
				} else if (arr[temp2] == null) {
					arr[temp2] = value;
					lastTemp = temp2;
					// System.out.println("Parent: "+myArr[temp2/2]+" value: "+value);
				}
				if (arr[temp1] != null && arr[temp2] != null)
					s++;

				arr = swap(lastTemp);

			}
		}
		// System.out.println(myArr[s]);

	}

	public int depth(String s) {
		int j = 0, count = -2;
		// System.out.println(links[m]);
		while (j < s.length()) {
			if (s.charAt(j) == '/') {
				count++;
			}
			j++;
		}
		return count;

	}

	public E[] swap(int num) {

		if (depth(arr[num].toString()) > (depth(arr[num / 2].toString()))) {
			
			E val = arr[num];
			arr[num] = arr[num / 2];
			arr[num / 2] = val;

			if (num / 2 >= 1)
				swap(num / 2);
		}

		return arr;

	}

	  
	public E dequeue() {
		//System.out.println("dequeue");
		E dVal = arr[1];
		arr[1] = arr[lastTemp];
		arr[lastTemp] = null;
		iFlag = 0;
		arr = dSwap();
		lastTemp--;
		//System.out.println("dval"+dVal);
		return dVal;
	}

	// int index=1;
	private E[] dSwap() {
		// System.out.println("===="+myArr[1]);
		if (iFlag != 1) {
			index = 1;
			iFlag = 1;
		}
		// System.out.println(index);
		// System.out.println("@@"+myArr[index]);
		// System.out.println("child "+myArr[2*index]+"----"+myArr[(2*index)+1]);
		if (arr[(2 * index)] != null && arr[(2 * index) + 1] != null) {
			if (arr[index].compareTo(arr[2 * index]) > 0) {
				E sVar = arr[index];
				arr[index] = arr[index * 2];
				arr[index * 2] = sVar;
				index = 2 * index;
			} else if (arr[index].compareTo(arr[(2 * index) + 1]) > 0) {
				E sVar = arr[index];
				arr[index] = arr[(2 * index) + 1];
				arr[(2 * index) + 1] = sVar;
				index = (2 * index) + 1;
			}
			if (arr[index] != null) {
				dSwap();
			}
		}
		return arr;
	}

	 
	public int size() {
		 
		int count = 0;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] != null)
				count++;
		}
		return count;
	}

	 
	public boolean is_empty() {
		 
		if (arr[1] == null)
			return true;

		return false;
	}

	 
	public E front() {
		 
		return arr[1];
	}

	public void display() {
		for (int k = 1; k < arr.length; k++)
			if (arr[k] != null)
				System.out.println(arr[k]);
	}
}
