/*package searchengine.spider;	import java.net.*;
import java.io.*;
import java.util.*;

import searchengine.parser.PageLexer;
import searchengine.url.URLTextReader;

	/** Web-crawling objects.  Instances of this class will crawl a given
	 *  web site in breadth-first order.
	 * THIS FILE IS FOR YOU TO FILL IN!!
	 
	public class test implements Spider {

	    /** Create a new web spider.
		@param u The URL of the web site to crawl.
		@param i The initial web index object to extend.
	     
	   
	    private Indexer i = null;
	    private URL u; 
		Queue1 q = new Queue1();

	    public BreadthFirstSpider (URL u, Indexer i) {
		this.u = u;
		this.i = i;
	    
	    }

	    /** Crawl the web, up to a certain number of web pages.
		@param limit The maximum number of pages to crawl.
	     
		
		 
	    public Indexer crawl (int limit) {
			System.out.println("Crawling Webpage ---------"+u);
			int flag=0;
		 Vector vec = new Vector();
		 Vector ext = new Vector();
	    	try
			{
				URLTextReader in = new URLTextReader(u);
				PageLexer pl = new PageLexer(in, u);
				int count = limit;
				while (pl.hasNext()) 
				{
					PageElement pe = (PageElement)pl.next();
				
					String std = pe.toString();
					if (pe instanceof PageHref&&!pe.toString().contains("#")&&count!=0)
					{
						int f = 0;
						String str=pe.toString();
						URL url= new URL(str);
						Qnode n = new Qnode(str);
						for (int i =0; i<ext.size();i++ )
						{
							if (ext.get(i).toString().equals(str))
							{
								f =1;
							}
						}
						if (f ==0)
						{
							q.enQueue(n);
							ext.add(n);
						count--;
						}
						
					}else 
						if(pe instanceof PageWord){
							String string = pe.toString();
							if(string.startsWith("<")){
								flag=1;
							}
							if(string.endsWith(">")){
								flag=0;}
							if(!string.endsWith(">")&&flag==0&&!string.contains("\t")&&!string.contains(" ")){
							vec.add(pe.toString());

							}
					}
				}
				ObjectIterator oi = new ObjectIterator(vec);
				i.addPage(u,oi);
				if(q.size()!=0){
				u = new URL(q.front.element);
				q.deQueue();
				System.out.println("*************URLs in the queue are***************");
				q.display();
			
				limit--;
				System.out.println(limit);
				if(limit>0)
					crawl(limit);
				}
			}
			catch(MalformedURLException e)
			{
				System.out.println("There is a url error" );
			}
			catch(IOException e)
			{
				try
				{
					System.out.println("IO exception thrown");
					if(q.front!=null){
					u = new URL(q.front.element);
					q.deQueue();
					System.out.println("*************URLs in the queue are***************");
					q.display();
					System.out.println(limit);
					 if(limit>0)
						 crawl(limit);}
				}catch(MalformedURLException e1)
				{
				}
			}
			catch(Exception e)
			{
				System.out.println("Exception thrown");
			}
	    	
		return i;
	    }
		
	    /** Crawl the web, up to the default number of web pages.
	     
	    public Indexer  crawl() {
		// This redirection may effect performance, but its OK !!
		return  crawl(crawlLimitDefault);
	    }

	    /** The maximum number of pages to crawl. 
	    public int crawlLimitDefault = 10;
	    
	}

}*/




/*/**  
 * 
 * Copyright: Copyright (c) 2004 Carnegie Mellon University
 * 
 * This program is part of an implementation for the PARKR project which is 
 * about developing a search engine using efficient Datastructures.
 * 
 * 
 */ 


//package src.searchengine;

/*import java.io.*;
import java.util.*;
import java.net.*;


/**
 * Web-indexing objects.  This class implements the Indexer interface
 * using a list-based index structure.

A Hash Map based implementation of Indexing 

 */
/*public class Indexer implements IndexerInterface
{
    /** The constructor for ListWebIndex.
     */

   // Index Structure 
	/*DictionaryInterface index;

   // This is for calculating the term frequency
    HashMap wordFrequency;

    public Indexer(String mode)
    {
	// hash - Dictionary Structure based on a Hashtable or HashMap from the Java collections 
	// list - Dictionary Structure based on Linked List 
	// myhash - Dictionary Struwcture based on a Hashtable implemented by the students
	// mytree - Dictionary Structure based on a Binary Search Tree implemented by the students

		if (mode.equals("hash")) 
			index = new HashDictionary();
		else if(mode.equals("list")){
			index = new ListDictionary();}
		else if(mode.equals("myhash"))
			index = new MyHash();
		else if(mode.equals("tree"))
			index = new TreeDictionary();
		/*
		 * For the project to compile you will have to create the two files ListDicionary.java and HashDictionary.java for this to work
		 * For you to fill in Task 1 - Tasklet A
		 * Later You will have to add more here to support "myhash" in Task3 
	/*	 */
    /*}
    /** Add the given web page to the index.
     *
     * @param url The web page to add to the index
     * @param keywords The keywords that are in the web page
     * @param links The hyperlinks that are in the web page
     */
    /*public void addPage(URL url, ObjectIterator keywords)
    {
		int freq,k,i=0;
		Vector vec=keywords.returnVec();
		//Object O,O1,ob;
		//String s1,s2;
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		HashMap hm;// = new HashMap();

			while(vec.size()>0)
			{
				wordFrequency = new HashMap();
				//O=v.get(0);
				String word=vec.get(0).toString();

				freq=1;
				for(i=1;i<(vec.size());i++)
				{
					//ob=v.get(i);
					String compare=vec.get(i).toString();
					if(word.equals(compare))
					{
						freq++;
						vec.remove(i);   
						i--;
					}   		
				}     			
				wordFrequency.put(url,freq);

				if(index.getValue(word)!=null)
				{    			
					hm=(HashMap) index.getValue(word);
					hm.putAll(wordFrequency);
					index.insert(word,hm);
				}
				else
				{

					index.insert(word,wordFrequency);

				}  

				vec.remove(0);
			}  
	
		
    }
    /** Produce a printable representation of the index.
     *
     * @return a String representation of the index structure
     */
    /* public String toString()
    {
	return "You dont need to implement it\n";
    }

    /** Retrieve all of the web pages that contain the given keyword.
     *
     * @param keyword The keyword to search on
     * @return An iterator of the web pages that match.
     */
    /*public ObjectIterator retrievePages(PageWord keyword)
    {
		Vector v = new Vector();
		String keys[]= index.getKeys();

		for (int i=0;i<keys.length ;i++ )
		{
			
			if ((keyword.toString()).equals(keys[i]))
			{
				Hashtable ht = (Hashtable)index.getValue(keys[i]);
				//Enumeration e = ht.keys();
				//while (e.hasMoreElements())
				//{

					v.add(ht);
				//}
			}
		}
		return new ObjectIterator(v);
    }

    /** Retrieve all of the web pages that contain any of the given keywords.
     *	
     * @param keywords The keywords to search on
     * @return An iterator of the web pages that match.
     * 
     * Calculating the Intersection of the pages here itself
     **/
    /*public ObjectIterator retrievePages(ObjectIterator keywords)
    {
		Vector v = keywords.returnVec();
		String ks[]= index.getKeys();
		Vector vv= new Vector();
		while (keywords.hasNext())
		{
			 Hashtable hd = new Hashtable();
			String k = keywords.next().toString();
			for (int i=0;i<ks.length ;i++ )
			{
				if(k.equals(ks[i])){
					 HashMap vals = (HashMap)index.getValue(ks[i]);
					 URL urls[]= new URL[vals.size()];
					 Set s = vals.keySet();
					 Object [] arr = s.toArray();
				
					 for (int j =0;j<urls.length ;j++ )
					 {   
						 urls[j]=(URL)arr[j];
						 hd.put(urls[j],vals.get(urls[j]));
					 }
				}
			}
			 Elmnt el = new Elmnt();
				el.word = k;
				el.val = hd;
				vv.add(el);
		}
	return new ObjectIterator(vv);
    }

    /** Save the index to a file.
     *
     * @param stream The stream to write the index
     */
    /*public void save(FileOutputStream stream) throws IOException
    {
		PrintStream pw = new PrintStream(stream);

		String a[]= index.getKeys();
		for (int i=0;i<a.length ;i++ )
	 {
		 HashMap hd = (HashMap)index.getValue(a[i]);
		 URL urls[]= new URL[hd.size()];
		 Set s = hd.keySet();
		 Object [] arr = s.toArray();	
		 for (int j =0;j<urls.length ;j++ )
		 {
			 urls[j]=(URL)arr[j];
		 }
		 for (int k=0;k<urls.length ;k++ )
		 {
			 pw.println(a[i]+"\t"+urls[k]+"\t"+hd.get(urls[k]));
		 }
	 }
		pw.close();
    }

    /** Restore the index from a file.
     *
     * @param stream The stream to read the index
     */
    /* public void restore(FileInputStream stream) throws IOException, MalformedURLException
    {
	//	System.out.println("restore started");
    	/*Scanner scan = new Scanner(stream);
		String tmp=null;
		String b=null;
		int c=0;
		while (scan.hasNext())
		{
			String a = scan.next();
		//	System.out.println(a+"\t");
			 b= scan.next();
		//	 System.out.print(b+"\t");
			 if(!scan.hasNextInt()){
				c=Integer.parseInt(b.toString());
				System.out.println(a);
				b=a;
				a="";
				}
			 if(scan.hasNextInt())
				c= scan.nextInt();
			 URL u = new URL(b);
		//	System.out.print(c);
			 /*HashMap nhm = new HashMap();
			nhm.put(u,c);
			if (index.getValue(a)!=null)
			{
				HashMap tmpmap = (HashMap)index.getValue(a);
				nhm.putAll(tmpmap);
			}
			index.insert(a,nhm);
			//System.out.println("ended");
			
		}
	
		System.out.println("After doing retrieval==========");
	
		
		save(new FileOutputStream(new File("foooo.txt")));
		System.out.println("After save");

    }

	/* Remove Page method not implemented right now
	 * @see src.parkr.Indexer#removePage(java.net.URL)
	 */
			 /*public void removePage(URL url) {
	}
};
class VecUrl
{
	String url;
	int freq;
	VecUrl(String a, int b){
		url = a;
		freq = b;
	}
}
class Elmnt 
{
	String word;
	Hashtable val;
}*/
