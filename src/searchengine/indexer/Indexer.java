/**  
 * 
 * Copyright: Copyright (c) 2004 Carnegie Mellon University
 * 
 * This program is part of an implementation for the PARKR project which is 
 * about developing a search engine using efficient Datastructures.
 * 
 * Modified by Mahender on 12-10-2009
 */ 

package searchengine.indexer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import searchengine.dictionary.AVLDictionary;
import searchengine.dictionary.BSTDictionary;
import searchengine.dictionary.DictionaryInterface;
import searchengine.dictionary.HashDictionary;
import searchengine.dictionary.ListDictionary;
import searchengine.dictionary.MyHashDictionary;
import searchengine.dictionary.ObjectIterator;
import searchengine.element.PageElementInterface;
import searchengine.element.PageWord;
import Set.MySet;


/**
 * Web-indexing objects.  This class implements the Indexer interface
 * using a list-based index structure.

A Hash Map based implementation of Indexing 

 */
public class Indexer implements IndexerInterface
{
	/** The constructor for ListWebIndex.
	 */

	// Index Structure 
	DictionaryInterface index;
	//HashMap wordFrequency;
	// This is for calculating the term frequency
	HashMap wordFrequency;

	public Indexer(String mode)
	{
		// hash - Dictionary Structure based on a Hashtable or HashMap from the Java collections 
		// list - Dictionary Structure based on Linked List 
		// myhash - Dictionary Structure based on a Hashtable implemented by the students
		// bst - Dictionary Structure based on a Binary Search Tree implemented by the students
		// avl - Dictionary Structure based on AVL Tree implemented by the students

		if (mode.equals("hash")) 
			index = new HashDictionary();
		else if(mode.equals("list"))
			index = new ListDictionary();
		else if(mode.equals("myhash"))
			index = new MyHashDictionary();
		else if(mode.equals("bst"))
			index = new BSTDictionary();
		else if(mode.equals("avl"))
			index = new AVLDictionary();
	}
	/** Add the given web page to the index.
	 *
	 * @param url The web page to add to the index
	 * @param keywords The keywords that are in the web page
	 * @param links The hyperlinks that are in the web page
	 */
	public void addPage(URL url, ObjectIterator keywords)
	{
		int freq,k,i=0;
		Vector vec=keywords.returnVec();
		//Object O,O1,ob;
		//String s1,s2;
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		HashMap hm;
		while(vec.size()>0)
		{
			wordFrequency = new HashMap();

			String word=vec.get(0).toString();

			freq=1;
			for(i=1;i<(vec.size());i++)
			{
				String next=vec.get(i).toString();
				if(word.equals(next))
				{
					freq++;
					vec.remove(i);   
					i--;
				}   		
			}     			
			wordFrequency.put(url,freq);
			if(index.getValue(word)!=null)
			{
				hm=(HashMap) index.getValue(word);
				hm.putAll(wordFrequency);
				index.insert(word,hm);
			}
			else
			{
				index.insert(word,wordFrequency);
			}  
			vec.remove(0);
		}
	}
	/** Produce a printable representation of the index.
	 *
	 * @return a String representation of the index structure
	 */
	public String toString()
	{
		////////////////////////////////////////////////////////////////////
		//  Write your Code here as part of Integrating and Running Mini Google assignment
		//  
		///////////////////////////////////////////////////////////////////
		return "You dont need to implement it\n";
	}

	/** Retrieve all of the web pages that contain the given keyword.
	 *
	 * @param keyword The keyword to search on
	 * @return An iterator of the web pages that match.
	 */
	public ObjectIterator<?> retrievePages(PageWord keyword)
	{
		////////////////////////////////////////////////////////////////////
		//  Write your Code here as part of Integrating and Running Mini Google assignment
		//  
		///////////////////////////////////////////////////////////////////
		//System.out.println("Key :"+keyword);
		Object value = index.getValue(keyword.toString());
		//System.out.println("Value1 :"+value);
		String str=(String) value.toString();
		//System.out.println("String :"+str);
		//System.out.println(str+"----Values---"+value);
		Vector<String> links =new Vector<String>();
		int flag=0;String r="";

		if(str!=null)
		{
			StringTokenizer tokens = new StringTokenizer(str, "!"); 
			while(tokens.hasMoreTokens())
			{
				String w=tokens.nextToken();

				//System.out.println("Value :"+w);
				if(flag==0)
				{
					r=w.substring(1);
					flag=1;
					//System.out.println("Key1 :"+r);
				}
				else
				{
					//System.out.println("String: "+w.substring(w.length()-1));
					r=w.replace(w.substring(w.length()-1),"");
					//r=w.substring(w.length()-1);
					//System.out.println("Key2 :"+r);
				}
				//w.con
				links.add(r);
				//System.out.println("next :"+r);
			}
		}



		return new ObjectIterator<String>(links);


		//return new ObjectIterator<PageElementInterface>(new Vector<PageElementInterface>());
	}

	/** Retrieve all of the web pages that contain any of the given keywords.
	 *	
	 * @param keywords The keywords to search on
	 * @return An iterator of the web pages that match.
	 * 
	 * Calculating the Intersection of the pages here itself
	 **/
	public ObjectIterator<?> retrievePages(ObjectIterator<?> keywords)
	{
		////////////////////////////////////////////////////////////////////
		//  Write your Code here as part of Integrating and Running Mini Google assignment
		//  
		///////////////////////////////////////////////////////////////////
		Vector<MySet<String>> allsets = new Vector<MySet<String>>();
		while(keywords.hasNext())
		{
			PageWord keyword = new PageWord(keywords.next().toString());
			ObjectIterator<String> links = (ObjectIterator<String>) retrievePages(keyword);
			Vector v=links.returnVec();
			allsets=v;
		}

		return new ObjectIterator<MySet<String>>(allsets);

	}

	/** Save the index to a file.
	 *
	 * @param stream The stream to write the index
	 */
	public void save(FileOutputStream stream) throws IOException
	{
		////////////////////////////////////////////////////////////////////
		//  Write your Code here as part of Integrating and Running Mini Google assignment
		//  
		///////////////////////////////////////////////////////////////////

		PrintStream pw = new PrintStream(stream);
		int l=1;
		Comparable[] a= index.getKeys();
		for (int i=0;i<a.length ;i++ )
		{
			HashMap hd = (HashMap)index.getValue(a[i]);
			//System.out.println(index.getValue(a[i]));
			URL links[]= new URL[hd.size()];
			//System.out.println("HD Size :"+hd.size());
			Set s = hd.keySet();
			Object [] arr = s.toArray();	
			for (int j =0;j<links.length ;j++ )
			{
				links[j]=(URL)arr[j];
			}
			pw.print(a[i]);

			for (int k=0;k<links.length ;k++ )
			{
				//System.out.println("   "+hd.get(links[k]));
				pw.print("!"+links[k]+"~~"+hd.get(links[k]));
			}

			pw.println();
			pw.println();
			l++;
		}
		pw.close();

	}

	/** Restore the index from a file.
	 *
	 * @param stream The stream to read the index
	 */
	public void restore(FileInputStream stream) throws IOException
	{
		////////////////////////////////////////////////////////////////////
		//  Write your Code here as part of Integrating and Running Mini Google assignment
		//  
		///////////////////////////////////////////////////////////////////
		try {
			InputStreamReader ir=new InputStreamReader(stream);
			BufferedReader br=new BufferedReader(ir);


			String line="";
			//int cnt = 0;

			while ((line=br.readLine())!=null)
			{
				// System.out.println("line : "+line);
				//cnt++;
				StringTokenizer st = new StringTokenizer(line,"!"); 
				//System.out.println("st: "+st);
				int flag=0;
				String key="";
				ArrayList<String> al=new ArrayList<String>();
				while(st.hasMoreElements())
				{   
					if(flag==0)
					{
						key=st.nextToken();
						flag++;
					}
					else
					{
						al.add(st.nextToken());
					}
				}

				index.insert(key,al);
				// System.out.println("keys:"+ key);
				//System.out.println("Array:"+ al);


			}
			//System.out.println("tot lines : "+cnt);
			br.close(); 

		} catch (IOException e) {

			System.out.println("in Indexr -  restore method : ");
			e.printStackTrace();
		}


	}

	/* Remove Page method not implemented right now
	 * @see searchengine.indexer#removePage(java.net.URL)
	 */
	public void removePage(URL url) {
	}
};