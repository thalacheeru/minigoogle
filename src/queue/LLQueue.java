package queue;


//import Stack.SLLNode;

public class LLQueue<T> implements QueueADT<T> {

	
class Node
{
	T val;
	Node next;
}
Node n;
Node first=null;int size=0;

	@Override
	public T enqueue(T element) {
		// TODO Auto-generated method stub
		n=new Node();
		n.val=element;
		n.next=first;
		
		first=n;
		size++;	
		return element;
		
	}

	@Override
	public T dequeue() {
		Node temp2 = null,temp1;
		T val = null;
		temp1 = n;
		if(temp1!=null)
		if(temp1.next==null)
		{
			val=temp1.val;
			n=null;
			//return n.val;
		}
		else
		{
		while(temp1.next!=null)
		{
			//System.out.println(temp1.val);
			temp2=temp1;
			temp1=temp1.next;
		}
		val=temp1.val;
		temp2.next=null;
		}
		size--;
		
		return val;
	}

	@Override
	public T front() {
		// TODO Auto-generated method stub
		T val = null;Node temp1=n;
		while(temp1.next!=null)
		{
			//System.out.println(temp1.val);
			temp1=temp1.next;
			
		}
		val=temp1.val;
		
		return val;
		
	}

	@Override
	public boolean isempty() {
		
		return (first==null);
	}

	@Override
	public int size() {
		
		return size;
	}
	
	public String toString()
	{
		Node temp1 = n;
		while(temp1!=null)
		{
			System.out.println(temp1.val);
			temp1 = temp1.next;
		}
		
		return "Q";
	}

	@Override
	public void Display() {
		
		
	}

}
/*public class LLQueue <T> implements QueueADT<T> 
{
	class node
	{
		T value;
		node next;
	}
	
	node first = null;
	node n,temp1;
	int size;
	
	public T front()
	{
		node temp1=n;
		while(temp1.next!=null)
			temp1=temp1.next;
		return temp1.value;
	}
	public T enqueue(T element)
	{
		n=new node();
		n.value=element;
		n.next=first;
			first=n;
		size++;
		return element;
	}
	public T dequeue()
	{
		node temp1;
		node temp2 = null;
		T element = null;
		temp1 = first;
		if (temp1.next != null) 
		{
			while (temp1.next != null) 
			{
				temp2 = temp1;
				temp1 = temp1.next;
			}
			element = temp1.value;
			temp2.next = null;
			size--;
		}
		else if(temp1!=null)
			return temp1.value;
		size--;
		return element;
		
	}
	public boolean isempty()
	{
		if(first==null)
			return true;
		else
			return false;
	}
	public int size() 
	{
		return size;
	}
	public void Display() 
	{
		int i=0;
		node m=first;
		node p=new node();
		while(m.next!=null)
		{if(i<=100)
			System.out.println(i+"  "+ m.value);
			m=m.next;i++;
		}//System.out.println(m.value);
		
		
		
	}
}/*package Stack;
public class SLStack<E> 
{
	int top;
	SLLNode<E> link;

	public SLStack()
	{
		top = 0;
		link = new SLLNode<E>();
	}

	public void push(E item) 
	{
		SLLNode<E> newnode = new SLLNode<E>(item);
		newnode.setNext(link);
		link = newnode;
	}

	public E pop() 
	{
		E data = link.getData();
		link = link.getNext();
		return data;
	}

	public E peek() 
	{
		E data = link.getData();
		return data;
	}

	public void Display() 
	{
		SLLNode<E> temp = link;
		while(temp.getNext() != null)
		{
			System.out.println(temp.getData());
			temp=temp.getNext();
		}
	}
}
*/