package queue;
public class CAQueue<T> implements QueueADT<T> 
{
	int first=0,size=0,max,del=0,pos=0;
	T[] Queue;

	public CAQueue()
	{
		max=100;
		Queue = (T[]) new Object[max]; 
	}

	public T front() 
	{
		if(size!=0)
			return Queue[first];
		else
			return null ; 
	}

	public T enqueue(T element) 
	{
		if(size==max)
		{
			size=0;
			//first=0;
			//System.out.println("Queue is Full Can Not Add Value");
		}
		//else
		if(element!=null && Queue[size]==null)
		{
			Queue[size]=element; 
			size++;
			pos++;//System.out.println("\nEntered");
		}
		//else
			//System.out.println("\nAll Are Filled");
		return element;
	}

	public T dequeue() 
	{
		//System.out.println("\n"+Queue[first]+" Deleted");
		T val=Queue[first];
		Queue[first]=null;
		first++;pos--;
		return val;
	}

	public boolean isempty()
	{
		if(size==0)
			return true;
		else
			return false;
	}

	public int size() 
	{
		return pos;
	}
	public void Display()
	{
		System.out.print("\nTotal Elements  \n");
		for(int i=0;i<size;i++)
		{
			System.out.println(Queue[i]);
		}//System.out.print("\n");
	}

}