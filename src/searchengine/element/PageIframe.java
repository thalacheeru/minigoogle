


	/**  
	 * 
	 * Copyright: Copyright (c) 2004 Carnegie Mellon University
	 * 
	 * This program is part of an implementation for the PARKR project which is 
	 * about developing a search engine using efficient Datastructures.
	 * 
	 * 
	 */ 

	package searchengine.element;

	import java.net.MalformedURLException;
	import java.net.URL;

	import searchengine.url.URLFixer;

	/** A img src in a web page.
	 *
	 */
	public class PageIframe implements PageElementInterface {

		public PageIframe (String h) throws MalformedURLException /* perhaps throw an invalid filename error? */{
			iframe = new URL(h);
		}
		public PageIframe (URL context, String h) throws MalformedURLException {
			iframe = URLFixer.fix(context, h);
		    }
		public  String toString () {
			return iframe.toString();
		}

		private URL iframe;
	}


